const express = require('express')
const app = express()

var quotes = [

    {
        "text": "Whatever you are, be a good one.", "author": "Abraham Lincoln",
        "tags": ["Famous Inspirational Quotes", "President Lincoln"]
    },
    {
        "text": "It has been my philosophy of life that difficulties vanish when faced boldly.", "author": "Isaac Asimov",
        "tags": ["Famous Inspirational Quotes", "Science Fiction"]
    },
    {
        "text": "Enjoy life. There’s plenty of time to be dead.", "author": "Anonymous",
        "tags": ["Famous Inspirational Quotes", "Anonymous Quote"]
    },
    {
        "text": "Every moment is a fresh beginning.", "author": "T.S. Eliot",
        "tags": ["Famous Inspirational Quotes"]
    },
    {
        "text": "One day your life will flash before your eyes. Make sure it is worth watching.", "author": "Anonymous",
        "tags": ["Famous Inspirational Quotes", "President Lincoln"]
    }

];

app.get('/random', (req, res) => {
    res.type('json');
    var index = Math.floor(Math.random() * quotes.length);
    var quote = quotes[index];
    res.send(JSON.stringify(quote, null, 2));
});

app.listen(3000, () => console.log('Quote service accepting requests on port 3000'))
