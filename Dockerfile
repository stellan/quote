FROM node:9-alpine

# Copy the application
ADD . /opt/app/

# Install node modules
WORKDIR /opt/app
RUN npm install

EXPOSE 3000
ENTRYPOINT node /opt/app/app.js
